import type { ResponseBody } from './callTypes';
import { ROUTE_BACKEND_LIST, ROUTE_BACKEND_LIST_ABILITY, ROUTE_BACKEND_VIEW } from '../constants/routeConstants';

// export function index(): Promise<ResponseBody> {
export function fetchPokemonListCall(query) {
  const request = new Request(ROUTE_BACKEND_LIST);

  return fetch(request)
    .then(response => response.json()
      .then(body => ({ status: response.status, body })),
    );
}

export function fetchPokemonViewCall(id) {
  const request = new Request(ROUTE_BACKEND_VIEW.replace(':id', id));

  return fetch(request)
    .then(response => response.json()
      .then(body => ({ status: response.status, body })),
    );
}

export function fetchPokemonAbilityCall(id) {
  const request = new Request(ROUTE_BACKEND_LIST_ABILITY.replace(':id', id));

  return fetch(request)
    .then(response => response.json()
      .then(body => ({ status: response.status, body })),
    );
}

export default {
  fetchPokemonListCall,
  fetchPokemonViewCall
};

