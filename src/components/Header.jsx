// @flow

import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import type { Dispatch } from 'redux';

import {
  ROUTE_HOME,
  ROUTE_FRONTEND_LIST, ROUTE_FRONTEND_ABOUT,
} from '../constants/routeConstants';

import type { RootState } from '../reducers/reducerTypes.js.flow';

class Header extends React.Component {


  render(): React$Node {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
        <div className="container">
          <Link to={ROUTE_HOME} className="navbar-brand" >Diego A.</Link>
          <button
            type="button"
            className="navbar-toggler"
            data-toggle="collapse"
            data-target="#main-nav"
            aria-controls="main-nav"
            aria-expanded="false"
          ><span className="navbar-toggler-icon" /></button>
          <div id="main-nav" className="collapse navbar-collapse">
            <ul className="navbar-nav">
              <NavLink exact to={ROUTE_HOME} className="nav-link" activeClassName="active">Home</NavLink>
              <NavLink to={ROUTE_FRONTEND_LIST} className="nav-link" activeClassName="active">THE POKEDEX</NavLink>
              <NavLink to={ROUTE_FRONTEND_ABOUT} className="nav-link" activeClassName="active">Sobre Mim</NavLink>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

function mapStateToProps(state: RootState): MappedState {
  return {  };
}

function mapDispatchToProps(dispatch: Dispatch<*>): MappedDispatch {
  return { };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
