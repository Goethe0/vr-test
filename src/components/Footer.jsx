// @flow

import React from 'react';

export default function Footer(): React$Node {
  return (
    <footer className="mt-4">
      <ul className="list-inline">
        <li className="list-inline-item">
          <a href={"https://www.linkedin.com/in/Goethe0"}>LinkedIn</a>
        </li>
        <div className="float-sm-right">
          <li className="list-inline-item">&copy; Diego A., {new Date().getFullYear()}</li>
        </div>
      </ul>
    </footer>
  );
}
