// @flow

import React from 'react';
import { ROUTE_FRONTEND_LIST } from '../constants/routeConstants';

export default function HomePage(): React$Node {
  return (
    <main>
      <div className="">
        <h1 className="display-3">Teste VR!</h1>
        <p>
          <a
            href={`#${ROUTE_FRONTEND_LIST}`}
            rel="noopener noreferrer"
            className="btn btn-primary btn-lg"
          >Pokedex</a>
        </p>
      </div>
    </main>
  );
}
