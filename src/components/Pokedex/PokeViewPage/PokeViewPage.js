import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import type { Dispatch } from 'redux';
import type { RootState } from '../../../reducers/reducerTypes';

import { fetchPokemonView } from '../../../actions/pokeActionCreators';
import {
  ROUTE_FRONTEND_LIST,
  ROUTE_FRONTEND_LIST_ABILITY,
  ROUTE_FRONTEND_VIEW,
  ROUTE_HOME
} from '../../../constants/routeConstants';

import { Breadcrumb, Col, Grid, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class PokeViewPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: false,
      name: '',
      pokemonView: {
        abilities: [],
        forms: [],
        game_indices: [],
        moves: [],
        species: [],
        sprites: [],
        stats: [],
        types: [],
        height: '',
        is_default: false,
        id: false,
        name: '',
        order: 0,
        weight: 0,
      },
    };
    this.goToTableAbilities = this.goToTableAbilities.bind(this);
  }

  componentDidMount(): void {
    const id = this.props.match.params.id;
    this.setState({id});
    this.props.pokeMethods.fetchPokemonView(id);
  }

  componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
    const pokemonView = nextProps.pokedex.pokemonView;
    if (this.state.pokemonView !== pokemonView) {
      this.setState({ pokemonView });
    }
  }

  goToTableAbilities(data) {
    if (!data.url) {
      return;
    }
    const dataArray = data.url.split("/");
    this.props.history.push(ROUTE_FRONTEND_LIST_ABILITY.replace(':id', dataArray[6]));
  }

  render() {
    const {
      id,
      name,
      pokemonView
    } = this.state;
    return (
      <div>
        <h2 className="hidden">Visualizar Pokemon</h2>
        <Breadcrumb>
          <Breadcrumb.Item href={`#${ROUTE_HOME}`}>
              Home ->
          </Breadcrumb.Item>
          <Breadcrumb.Item href={`#${ROUTE_FRONTEND_LIST}`}>
            &nbsp;
            Pokedex ->
          </Breadcrumb.Item>
          <Breadcrumb.Item active>
            &nbsp;
            Visualizar Pokemon
          </Breadcrumb.Item>
        </Breadcrumb>


        {id && (
        <Grid>
          <Row>
            <Col md={12}>
              <Link
                className='btn btn-dark'
                to={ROUTE_FRONTEND_LIST}
              >
                Retornar a lista
              </Link>
              <h2 className='pokeview-title'>
                {pokemonView.name}
              </h2>
            </Col>
          </Row>
          <Row>
            {Object.keys(pokemonView.sprites).map((item, i) => {
              if (pokemonView.sprites[item] == null || pokemonView.sprites[item] == '') {
                return;
              }
              return (
                <Col md={3} key={i+'img' + i}>
                  <img
                    className='pokemon-img-sprite'
                    src={pokemonView.sprites[item]}

                   />
                </Col>
              )
            })}
          </Row>
          <Row>
            <Col md={4}>
              <span className='pokeview-label strong'>
                <h4>Informações:</h4>
              </span>
              <span className='pokeview-data'>
                <strong>ID: </strong>{pokemonView.id} <br />
                <strong>Nome: </strong>{pokemonView.name} <br />
                <strong>Ordem: </strong>{pokemonView.order} <br />
                <strong>Altura: </strong>{pokemonView.height} <br />
                <strong>Peso: </strong>{pokemonView.weight} <br />
              </span>
            </Col>
            <Col md={4}>
              <span className='pokeview-label strong'>
                <h4>Lista de Habilidades:</h4>
              </span>
              {pokemonView.abilities.map((ability) => (
              <span
                className='pokeview-data btn-link'
                key={ability.ability.name} onClick={() => this.goToTableAbilities(ability.ability)}
              >
                Slot: {ability.slot} - Habilidade: {ability.ability.name} <br />

              </span>))}
            </Col>
          </Row>
        </Grid>)}
      </div>
    );
  }
}

function mapStateToProps(state: RootState): MappedState {
  return { pokedex: state.pokedex };
}

function mapDispatchToProps(dispatch: Dispatch<*>): MappedDispatch {
  return { pokeMethods: bindActionCreators({
      fetchPokemonView
    }, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(PokeViewPage);
