import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import type { Dispatch } from 'redux';
import type { RootState } from '../../../reducers/reducerTypes';

import { fetchPokemonAbility } from '../../../actions/pokeActionCreators';
import { ROUTE_FRONTEND_LIST, ROUTE_FRONTEND_VIEW, ROUTE_HOME } from '../../../constants/routeConstants';

import { Breadcrumb, Col, Grid, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PokeListTable from '../PokeListPage/PokeListTable';

class PokeListAbilityPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: false,
      name: '',
      pokemonListAbility: {
        id: false,
        name: '',
        pokemon: []
      },
    };
    this.goToView = this.goToView.bind(this);
  }

  componentDidMount(): void {
    const id = this.props.match.params.id;
    this.setState({id});
    this.props.pokeMethods.fetchPokemonAbility(id);
  }

  componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
    const pokemonListAbility = nextProps.pokedex.pokemonListAbility;
    if (this.state.pokemonListAbility !== pokemonListAbility) {
      this.setState({ pokemonListAbility });
    }
  }

  goToView(data) {
    if (!data.pokemon.url) {
      return;
    }
    const dataArray = data.pokemon.url.split("/");
    this.props.history.push(ROUTE_FRONTEND_VIEW.replace(':id', dataArray[6]));
  }

  render() {
    const {
      id,
      name,
      pokemonListAbility
    } = this.state;
    return (
      <div>
        <h2 className="hidden">Lista de Pokemon por Habilidade</h2>
        <Breadcrumb>
          <Breadcrumb.Item href={`#${ROUTE_HOME}`}>
            Home ->
          </Breadcrumb.Item>
          <Breadcrumb.Item href={`#${ROUTE_FRONTEND_LIST}`}>
            &nbsp;
            Pokedex ->
          </Breadcrumb.Item>
          <Breadcrumb.Item active>
            &nbsp;
            Lista de Pokemon por Habilidade
          </Breadcrumb.Item>
        </Breadcrumb>


        {id && (
          <Grid>
            <Row>
              <Col md={12}>
                <Link
                  className='btn btn-dark'
                  to={ROUTE_FRONTEND_LIST}
                >
                  Retornar a lista
                </Link>
                <h2 className='pokeview-title'>
                  {pokemonListAbility.name}
                </h2>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <PokeListTable
                  pokemonList={pokemonListAbility.pokemon}
                  goToView={this.goToView}
                />
              </Col>
            </Row>
          </Grid>)}
      </div>
    );
  }
}

function mapStateToProps(state: RootState): MappedState {
  return { pokedex: state.pokedex };
}

function mapDispatchToProps(dispatch: Dispatch<*>): MappedDispatch {
  return { pokeMethods: bindActionCreators({
      fetchPokemonAbility
    }, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(PokeListAbilityPage);
