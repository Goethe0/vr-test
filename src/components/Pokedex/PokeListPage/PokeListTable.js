import React from 'react';

import { ButtonToolbar, Table, Button, Breadcrumb } from 'react-bootstrap';
import { ROUTE_FRONTEND_LIST, ROUTE_HOME } from '../../../constants/routeConstants';
import { Link } from 'react-router-dom';

export const PokeListTable = ({pokemonList, goToView}) => {

  return (
    <div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th colSpan={3}>Nome do Pokemon</th>
            <th>Ação</th>
          </tr>
        </thead>
        <tbody>
          {pokemonList.map((item) => (
            <tr key={item.name ? item.name : item.pokemon.name}>
              <td colSpan={3} className='pokeview-title'>
                {item.name && item.name}
                {item.pokemon && item.pokemon.name}
              </td>
              <td>
                <ButtonToolbar>
                  <Button
                    bsStyle="primary"
                    onClick={() => goToView(item)}
                  >
                    Visualizar
                  </Button>
                </ButtonToolbar>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

    </div>
  );
};

export default PokeListTable;
