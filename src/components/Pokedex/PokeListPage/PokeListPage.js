import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import type { Dispatch } from 'redux';
import type { RootState } from '../../../reducers/reducerTypes';

import { fetchPokemonList } from '../../../actions/pokeActionCreators';
import PokeListTable from './PokeListTable';
import { ROUTE_FRONTEND_VIEW, ROUTE_HOME } from '../../../constants/routeConstants';

import { Breadcrumb} from 'react-bootstrap';

class PokeListPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pokemonList: {
        count: 0,
        next: '',
        previous: '',
        results: [],
      }
    };
    this.goToView = this.goToView.bind(this);
  }

  componentDidMount(): void {
    this.props.pokeMethods.fetchPokemonList([]);
  }

  componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
    const pokemonList = nextProps.pokedex.pokemonList;
    if (this.state.pokemonList !== pokemonList) {
      this.setState({ pokemonList });
    }
  }

  goToView(data) {
    if (!data.url) {
      return;
    }
    const dataArray = data.url.split("/");
    this.props.history.push(ROUTE_FRONTEND_VIEW.replace(':id', dataArray[6]));
  }

  render() {
    const {
      pokemonList
    } = this.state;

    return (
      <div>
        <h2>Lista de Pokemons</h2>
        <Breadcrumb>
          <Breadcrumb.Item href={`#${ROUTE_HOME}`}>
            Home ->
          </Breadcrumb.Item>
          <Breadcrumb.Item active>
            &nbsp;
            Pokedex
          </Breadcrumb.Item>
        </Breadcrumb>
        <PokeListTable
          pokemonList={pokemonList.results}
          goToView={this.goToView}
        />
      </div>
    );
  }
}

function mapStateToProps(state: RootState): MappedState {
  return { pokedex: state.pokedex };
}

function mapDispatchToProps(dispatch: Dispatch<*>): MappedDispatch {
  return { pokeMethods: bindActionCreators({
      fetchPokemonList
      }, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(PokeListPage);
