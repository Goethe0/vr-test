import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

class About extends Component {

  render() {
    return(
      <div>
        <Row>
          <Col md={4} className={'text-center'}>
            <img
              className='about-img-avatar'
              src='assets/0.jpeg'
            /><br /><br />
            <a
              href={"https://www.linkedin.com/in/Goethe0"}
              className={'btn btn-success'}
              target={'_blank'}
            >LinkedIn</a>
          </Col>
          <Col md={8}>
            <span>
              <strong>Full-Stack</strong><br />
              - Backend: PHP (Sênior)<br />
              -- Frameworks PHP: Symfony, Laravel, Yii, etc...<br />
              - Frontend: JS: ReactJS, Angular, jQuery, Bootstrap entre outros.<br /><br />
              <strong>Mobile</strong><br />
              - React Native<br />
              - Ionic<br /><br />
              - Fluente em Inglês, Alemão básico e Português nativo <br />
              - 18 anos c/ Web Development<br />
              - 7 anos c/ Mobile Development<br /><br />
            </span>
          </Col>
        </Row>
      </div>
    );
  };
}

export default About;
