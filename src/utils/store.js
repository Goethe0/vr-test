// @flow

import { combineReducers, applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';

import appReducer from '../reducers/appReducer';
import pokeReducer from '../reducers/pokeReducer';
import history from './history';

const rootReducer = combineReducers({
  router: routerReducer,
  app: appReducer,
  pokedex: pokeReducer
});
const enhancer = composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history)));

export default createStore(rootReducer, enhancer);
