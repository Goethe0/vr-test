// @flow

export const URL_API_BASE = 'http://api.goethedev.com:8080';

export const URL_APP_ACCOUNT_ACTIVATION_CONFIRMATION = 'http://goethedev.com:8080/#/register';
export const URL_APP_PASSWORD_RESET = 'http://goethedev.com:8080/#/restore-password';
export const URL_APP_EMAIL_CHANGE_CONFIRMATION = 'http://goethedev.com:8080/#/settings';

export default {
  URL_API_BASE,

  URL_APP_ACCOUNT_ACTIVATION_CONFIRMATION,
  URL_APP_PASSWORD_RESET,
  URL_APP_EMAIL_CHANGE_CONFIRMATION,
};
