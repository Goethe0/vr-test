// @flow

export const ROUTE_HOME = '/';

export const ROUTE_BACKEND_LIST = 'https://pokeapi.co/api/v2/pokemon/';
export const ROUTE_BACKEND_LIST_ABILITY = 'https://pokeapi.co/api/v2/ability/:id';
export const ROUTE_BACKEND_VIEW = 'https://pokeapi.co/api/v2/pokemon/:id';

export const ROUTE_FRONTEND_LIST = '/pokemon';
export const ROUTE_FRONTEND_LIST_ABILITY = '/pokemon/abilities/:id';
export const ROUTE_FRONTEND_VIEW = '/pokemon/:id';
export const ROUTE_FRONTEND_ABOUT = '/about';


export default {
  ROUTE_HOME,

  ROUTE_BACKEND_LIST,
  ROUTE_BACKEND_LIST_ABILITY,
  ROUTE_BACKEND_VIEW,
  ROUTE_FRONTEND_LIST,
  ROUTE_FRONTEND_VIEW,
  ROUTE_FRONTEND_ABOUT,
  ROUTE_FRONTEND_LIST_ABILITY
};
