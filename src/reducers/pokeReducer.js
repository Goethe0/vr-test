// @flow

import { LOCATION_CHANGE } from 'react-router-redux';

import {
  LOADING_FALSE,
  LOADING_TRUE,
  POKEDEX_LIST,
  POKEDEX_LIST_ABILITY,
  POKEDEX_VIEW
} from '../constants/actionTypeConstants';

const initialState = {
  loading: false,
  error: false,
  pokemonList: {
    count: 0,
    next: '',
    previous: '',
    results: [],
  },
  pokemonListAbility: {
    id: false,
    name: '',
    pokemon: []
  },
  pokemonView: {
    abilities: [],
    forms: [],
    game_indices: [],
    moves: [],
    species: [],
    sprites: [],
    stats: [],
    types: [],
    height: '',
    is_default: false,
    id: false,
    name: '',
    order: 0,
    weight: 0,
  },
};

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case LOADING_TRUE:
      return {
        ...state,
        loading: true,
      };
    case LOADING_FALSE:
      return {
        ...state,
        loading: false,
      };
    case POKEDEX_LIST:
      return {
        ...state,
        loading: false,
        pokemonList: (action.body) ? action.body : [],
      };
    case POKEDEX_LIST_ABILITY:
      return {
        ...state,
        loading: false,
        pokemonListAbility: (action.body) ? action.body : [],
      };
    case POKEDEX_VIEW:
      return {
        ...state,
        loading: false,
        pokemonView: (action.body) ? action.body : [],
      };
    default:
      return state;
  }
}
