import {
  LOADING_TRUE,
  POKEDEX_LIST,
  POKEDEX_LIST_ABILITY,
  POKEDEX_VIEW
} from '../constants/actionTypeConstants';
import { fetchPokemonAbilityCall, fetchPokemonListCall, fetchPokemonViewCall } from '../api/pokeCalls';

export function fetchPokemonList(query) {
  return (dispatch: Dispatch) => {
    dispatch({ type: LOADING_TRUE });
    fetchPokemonListCall(query)
      .then(response => {
        switch (response.status) {
          case 200:
            dispatch({
              type: POKEDEX_LIST,
              body: response.body
            });
            break;
          default:
            throw new Error('Unexpected response returned.');
        }
      });
  };
}

export function fetchPokemonView(id) {
  return (dispatch: Dispatch) => {
    dispatch({type: LOADING_TRUE});
    fetchPokemonViewCall(id).then(response => {
      switch (response.status) {
        case 200:
          dispatch({
            type: POKEDEX_VIEW,
            body: response.body
          });
          break;
        default:
          throw new Error('Unexpected response returned.');
      }
    });
  };
}

export function fetchPokemonAbility(id) {
  return (dispatch: Dispatch) => {
    dispatch({type: LOADING_TRUE});
    fetchPokemonAbilityCall(id).then(response => {
      switch (response.status) {
        case 200:
          dispatch({
            type: POKEDEX_LIST_ABILITY,
            body: response.body
          });
          break;
        default:
          throw new Error('Unexpected response returned.');
      }
    });
  };
}

export default {
  fetchPokemonList,
  fetchPokemonView,
  fetchPokemonAbility
}
